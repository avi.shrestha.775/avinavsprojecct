package com.ATC.Contact.controller;
import com.ATC.Contact.exception.InfoNotFoundException;
import com.ATC.Contact.model.Info;
import com.ATC.Contact.repository.InfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import java.util.List;

@RestController
public class InfoController {

@Autowired
    InfoRepository InfoRepository;

// Get All Notes
@GetMapping("/infos")
public List<Info> getAllNotes() {
    return InfoRepository.findAll();
}

//Create a new Note
@PostMapping("/infos")
public Info createNote(@RequestBody Info info) {
    return InfoRepository.save(info);
}

//Get a Single Note
@GetMapping("/infos/{customer_id}")
public Info getNoteById(@PathVariable(value = "cusotmer_id") Long customer_id) throws InfoNotFoundException {
    return InfoRepository.findById(customer_id)
            .orElseThrow(() -> new InfoNotFoundException(customer_id));
}

//Update a Note
@PutMapping("/infos/{customer_id}")
public Info updateNote(@PathVariable(value = "cusotmer_id") Long cusotmer_id,
                      @RequestBody Info infoDetails) throws InfoNotFoundException {

Info info = InfoRepository.findById(cusotmer_id)
            .orElseThrow(() -> new InfoNotFoundException(cusotmer_id));

info.setCustomer_fullName(infoDetails.getCustomer_fullName());
    info.setEmail(infoDetails.getEmail());
    info.setSubject(infoDetails.getSubject());

Info updatedInfo = InfoRepository.save(info);

return updatedInfo;
}

//Delete a Note
@DeleteMapping("/infos/{customer_id}")
public ResponseEntity<?> deleteInfo(@PathVariable(value = "customer_id") Long customer_id) throws InfoNotFoundException {
    Info info = InfoRepository.findById(customer_id)
            .orElseThrow(() -> new InfoNotFoundException(customer_id));

InfoRepository.delete(info);

return ResponseEntity.ok().build();
}
}