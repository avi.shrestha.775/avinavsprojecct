package com.ATC.Contact.repository;

import com.ATC.Contact.model.Info;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface InfoRepository extends JpaRepository<Info, Long>{

}

