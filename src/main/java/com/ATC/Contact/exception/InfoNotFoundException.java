package com.ATC.Contact.exception;

public class InfoNotFoundException extends Exception {
private long customer_id;
public InfoNotFoundException(long customer_id) {
        super(String.format("Customer Info is not found with id : '%s'",customer_id));
        }
}
