package com.ATC.Contact.model;

import javax.persistence.*;


@Entity
@Table(name = "books")

public class Info {
    @Id
    @GeneratedValue
    private Long customer_id;

    private String customer_fullName;

    private String email;
    private String subject;
    private String messages;

    public Info(){
      super();
  }
public Info(Long customer_id, String customer_fullName, String email, String subject, String messages) {
      super();
      this.customer_id = customer_id;
      this.customer_fullName =customer_fullName;
      this.email = email;
      this.subject=subject;
      this.messages = messages;
  }
    
    public Long getCustomer_id() {
		return customer_id;
	}
	public void setId(Long customer_id) {
		this.customer_id = customer_id;
	}
	public String getCustomer_fullName() {
		return customer_fullName;
	}
	public void setCustomer_fullName(String customer_fullName) {
		this.customer_fullName = customer_fullName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMessages() {
		return messages;
	}
	public void setMessages(String messages) {
		this.messages = messages;
	}
    
}

