package sqlConnection;

import java.sql.*;
import javax.swing.*;
public class curd_DB {
	Connection con;
	Statement stmt;
	public curd_DB() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306";
			con = DriverManager.getConnection(url,"root","");
			
			if(con!=null) {
				JOptionPane.showMessageDialog(null,"Connected");
			}
			else {
				JOptionPane.showMessageDialog(null,"Not Connected");	
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void createDB() {
		String sql = "CREATE schema ContactInfo";
		try {
		stmt = con.createStatement();
		int rs = stmt.executeUpdate(sql);
		
		if(rs!=1) {
			JOptionPane.showMessageDialog(null,"Database Created");
		}
	}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void createTable() {
		String sql = "CRAETE table customers(customer_id int(3) primary key unique,customer_fullName varchar(75) not null,customer_email varchar(255) not null,customer_subject varchar(255) not null,customer_messages varchar(255) not null)";
		try {
			stmt = con.createStatement();
			int rs = stmt.executeUpdate(sql);
			if(rs!=1) {
				JOptionPane.showMessageDialog(null,"Database Created");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void viewDB() {
		String url  = "Select * FROM customers";
		try {
			stmt  = con.createStatement();
			ResultSet rs = stmt.executeQuery(url);
			while(rs.next()) {
				System.out.println("Id:"+rs.getInt("customer_id"));
				System.out.println("Customer Name:"+rs.getString("customer_fullName"));
				System.out.println("Email:"+rs.getString("customer_email"));
				System.out.println("Subject:"+rs.getString("customer_subject"));
				System.out.println("Messages:"+rs.getString("customer_messages"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void close() {
		try {
			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void psvm() {
		curd_DB c = new curd_DB();
		c.createDB();
		c.createTable();
		c.viewDB();
		c.close();
}
	
	
}
